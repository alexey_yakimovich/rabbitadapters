﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WhiteRabbitUIClient
{
    public partial class DemoClient : Form
    {
        delegate void SetTextCallback(string text);
        Rabbit.White client;
        int Row = 1;
        int Col = 1;
        public DemoClient()
        {
            InitializeComponent();
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            pictureBox.Image = Properties.Resources.rabbit;
            client = new Rabbit.White(this.address.Text);
            client.onMessageReceived += client_onMessageReceived;
            client.Subscribe();
        }

        void client_onMessageReceived(object sender, Rabbit.MessageReceiveArgs e)
        {
            if (this.pictureBox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(UpdateRabbit);
                this.Invoke(d, new object[] { e.Message });
            }
            else
            {
                UpdateRabbit(e.Message);
            }
        }

        private void UpdateRabbit(string text)
        {
            if (text == "UP")
                Row--;
            else if (text == "DOWN")
                Row++;
            else if (text == "LEFT")
                Col--;
            else if (text == "RIGHT")
                Col++;
            if (Row > 0)
                Row %= 3;
            else
                Row = 2;
            if (Col > 0)
                Col %= 3;
            else
                Col = 2;

            this.pictureBox.Location = new Point(28 + 106 * Col, 57 + 106 * Row);
        }

        private void DemoClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            client.StopSubscription();
        }
    }
}

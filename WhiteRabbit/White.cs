﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Alchemy;
using Alchemy.Classes;
using System.Net;

namespace Rabbit
{
    public class MessageReceiveArgs : EventArgs
    {
        public string Message;
    }

    public delegate void MessageHandler(object sender, MessageReceiveArgs e);
    public class White
    {
        ConnectionFactory factory;
        Thread thread;
        public event MessageHandler onMessageReceived;
        private bool stopSubscription = false;
        private WebSocketServer wsServer;
        private List<UserContext> wsClients = new List<UserContext>();
        private IConnection connection;
        private IModel channel;

        public White()
        {
            factory = new ConnectionFactory() { HostName = "localhost" };
            thread = new Thread(new ThreadStart(SubscribeListener));
        }

        public White(string HostName)
        {
            factory = new ConnectionFactory() { HostName = HostName };
            thread = new Thread(new ThreadStart(SubscribeListener));
        }

        public void StartWSServer()
        {
            this.wsServer = new WebSocketServer(11111, IPAddress.Any)
            {
                TimeOut = new TimeSpan(1, 0, 0),
                OnConnect = onSocketClient,
                OnDisconnect = onSocketClientDrop
            };
            wsServer.Start();
        }

        private void onSocketClientDrop(UserContext context)
        {
            wsClients.Remove(context);
        }

        private void onSocketClient(UserContext context)
        {
            Console.WriteLine("Client connected");
            wsClients.Add(context);
        }

        private void SubscribeListener()
        {
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare("actions", "fanout");

                    var queueName = channel.QueueDeclare().QueueName;

                    channel.QueueBind(queueName, "actions", "");
                    var consumer = new QueueingBasicConsumer(channel);
                    channel.BasicConsume(queueName, true, consumer);
                    while (true)
                    {
                        var ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();

                        var body = ea.Body;
                        var message = Encoding.UTF8.GetString(body);
                        if (message == "halt" || stopSubscription)
                            break;
                        if (onMessageReceived != null)
                            onMessageReceived(this, new MessageReceiveArgs() { Message = message });
                    }
                }
            }
        }

        public void StopSubscription()
        {
            stopSubscription = true;
            thread.Abort();
        }

        public void Publish(string Event)
        {
            if (connection == null)
                connection = factory.CreateConnection();
            if (channel == null)
                channel = connection.CreateModel();

            channel.ExchangeDeclare("actions", "fanout");

            var body = Encoding.UTF8.GetBytes(Event);
            channel.BasicPublish("actions", "", null, body);
            foreach (UserContext context in wsClients)
                context.Send(Event);
        }

        public void Subscribe()
        {
            stopSubscription = false;
            thread.Start();
        }
    }
}

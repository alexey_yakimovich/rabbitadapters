﻿using System;

namespace WhiteRabbitServer
{
    public class PublisherService : IPublisher
    {
        private static Rabbit.White rabbit;

        public PublisherService()
        {
            if (rabbit == null)
            {
                rabbit = new Rabbit.White();
                rabbit.StartWSServer();
            }
        }

        public void Publish(string Message)
        {
            Console.WriteLine(Message);
            rabbit.Publish(Message);
        }
    }
}

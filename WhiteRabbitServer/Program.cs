﻿using System;
using System.ServiceModel;

namespace WhiteRabbitServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri baseAddress = new Uri("http://localhost:8181/white_rabbit");

            using (ServiceHost host = new ServiceHost(typeof(PublisherService), baseAddress))
            {

                host.AddServiceEndpoint(typeof(IPublisher), new BasicHttpBinding(), "Service");

                host.Open();

                Console.WriteLine("The service is ready at {0}", baseAddress);
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                // Close the ServiceHost.
                host.Close();
            }
        }
    }
}

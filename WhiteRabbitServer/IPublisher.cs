﻿using System.ServiceModel;

namespace WhiteRabbitServer
{
    [ServiceContract]
    public interface IPublisher
    {
        [OperationContract]
        void Publish(string Message);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteRabbitAdapter
{
    class Program
    {
        static void Main(string[] args)
        {
            Rabbit.White rabbit = new Rabbit.White();
            rabbit.onMessageReceived += rabbit_onMessageReceived;
            rabbit.Subscribe();
        }

        static void rabbit_onMessageReceived(object sender, Rabbit.MessageReceiveArgs e)
        {
            using (Subscriber.SubscriberPortTypeClient service = new Subscriber.SubscriberPortTypeClient("SubscriberSoap"))
            {
                service.PushAction(e.Message);
            }
        }
    }
}
